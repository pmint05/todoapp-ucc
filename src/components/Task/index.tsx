import { TaskProps } from "../../constants/interfaces";
import { Status, Priority } from "../../constants/enums";
import {
	CheckOutlined,
	DeleteOutlined,
	EditOutlined,
	FireTwoTone,
	MoreOutlined,
	PushpinOutlined,
} from "@ant-design/icons";
import { ConfigProvider, Dropdown, MenuProps } from "antd";
const Task = ({
	todo,
	onEdit,
	onDelete,
	markDone,
	markInprogress,
}: TaskProps) => {
	let iconStyle;
	let style;
	const { title, priority, status, updateAt } = todo;
	switch (priority) {
		case Priority.Low:
			iconStyle = "saturate-0";
			break;
		case Priority.High:
			iconStyle = "hue-rotate-[140deg]";
			break;
		case Priority.Medium:
			iconStyle = "";
	}
	switch (status) {
		case Status.Todo:
			style = "";
			break;
		case Status.Inprogress:
			style = "ring-2 ring-blue-300";
			break;
		case Status.Done:
			style = "!bg-green-50 ring-2 ring-green-300 line-through";
	}
	const items: MenuProps["items"] = [
		{
			label: (
				<button
					onClick={() => onEdit(todo)}
					className="px-2 py-1 block w-full text-left"
				>
					<EditOutlined /> Edit
				</button>
			),
			key: "0",
		},
		{
			type: "divider",
		},
		{
			label: (
				<button
					className="px-2 py-1 block w-full text-left transition-colors hover:text-red-500"
					onClick={() => onDelete(todo)}
				>
					<DeleteOutlined /> Delete
				</button>
			),
			key: "2",
		},
	];
	const isDone =
		status === Status.Done ? "!bg-green-200 !text-green-500" : "";
	const isInprogress =
		status === Status.Inprogress ? "!bg-blue-200 !text-blue-500" : "";
	return (
		<div
			className={`bg-white rounded-lg shadow-sm transition-shadow hover:shadow-md p-4 md:min-w-72 min-w-60 ${style}`}
		>
			<div className="flex items-start justify-between">
				<div className="flex items-center mr-3">
					<FireTwoTone className={`mr-3 ${iconStyle}`} />
					<h3 className="font-medium text-lg md:w-60 w-36 break-words">
						{title}
					</h3>
				</div>
				<div className="flex items-center gap-1">
					<button
						className={`text-gray-500 h-6 w-6 flex items-center justify-center rounded-md hover:bg-green-100 hover:text-green-500 transition-all ${isDone}`}
						onClick={() => markDone(todo.id)}
					>
						<CheckOutlined />
					</button>
					<button
						className={`text-gray-500 h-6 w-6 flex items-center justify-center rounded-md hover:bg-blue-100 hover:text-blue-500 transition-all ${isInprogress}`}
						onClick={() => markInprogress(todo.id)}
					>
						<PushpinOutlined />
					</button>
					<ConfigProvider
						theme={{
							components: {
								Dropdown: {
									paddingBlock: 0,
									controlPaddingHorizontal: 0,
								},
							},
						}}
					>
						<Dropdown
							menu={{ items }}
							trigger={["click"]}
							className="text-gray-500 h-6 w-6 flex items-center justify-center rounded-md hover:bg-gray-100 transition-all rotate-90"
						>
							<MoreOutlined />
						</Dropdown>
					</ConfigProvider>
				</div>
			</div>
			<div
				className={`block w-fit ml-auto font-medium text-sm mt-2 text-gray-400 bg-gray-100 px-2 py-1 rounded-md ${isDone} ${isInprogress}`}
			>
				{new Date(updateAt).toLocaleString()}
			</div>
		</div>
	);
};

export default Task;
