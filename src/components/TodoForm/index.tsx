import { ConfigProvider, Form, Input, Radio } from "antd";
import { useEffect } from "react";
import { Priority, Status } from "../../constants/enums";
import { TodoFormProps } from "../../constants/interfaces";

const TodoForm = ({ onFormInstanceReady, initialValues }: TodoFormProps) => {
	const [form] = Form.useForm();
	let inputRef = null;
	const setInputRef = (element: any) => {
		inputRef = element;
		if (element) {
			element.focus();
		}
	};

	useEffect(() => {
		onFormInstanceReady(form);
	}, []);

	return (
		<ConfigProvider
			theme={{
				components: {
					Form: {
						labelColor: "#888",
						itemMarginBottom: 20,
					},
				},
			}}
		>
			<Form
				className="max-w-sm"
				autoComplete="off"
				spellCheck={false}
				layout="vertical"
				form={form}
				initialValues={initialValues}
			>
				<Form.Item hidden name="createAt">
					<Input />
				</Form.Item>
				<Form.Item hidden name="updateAt">
					<Input />
				</Form.Item>
				<Form.Item
					name="title"
					label="Title"
					rules={[
						{
							required: true,
							message: "Please input your title!",
							min: 3,
						},
					]}
				>
					<Input placeholder="Title..." ref={setInputRef} />
				</Form.Item>
				<Form.Item name="priority" label="Priority">
					<Radio.Group>
						<Radio value={Priority.High}>High</Radio>
						<Radio value={Priority.Medium}>Medium</Radio>
						<Radio value={Priority.Low}>Low</Radio>
					</Radio.Group>
				</Form.Item>
				<Form.Item name="status" label="Status">
					<Radio.Group>
						<Radio value={Status.Todo}>Todo</Radio>
						<Radio value={Status.Inprogress}>Inprogress</Radio>
						<Radio value={Status.Done}>Done</Radio>
					</Radio.Group>
				</Form.Item>
			</Form>
		</ConfigProvider>
	);
};

export default TodoForm;
