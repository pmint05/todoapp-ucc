import { useState } from "react";
import { TodoModalProps } from "../../constants/interfaces";
import { FormInstance, Modal } from "antd";
import TodoForm from "../TodoForm";

const TodoModal = ({
	open,
	onCreate,
	onCancel,
	initialValues,
}: TodoModalProps) => {
	const [formInstance, setFormInstance] = useState<FormInstance>();
	return (
		<Modal
			className="max-w-sm"
			open={open}
			title={
				initialValues.id
					? `Edit todo ${initialValues.id}`
					: "Create a new todo"
			}
			okText={initialValues.id ? "Update" : "Create"}
			cancelText="Cancel"
			okButtonProps={{ autoFocus: true }}
			onCancel={onCancel}
			destroyOnClose
			onOk={async () => {
				try {
					const values = await formInstance?.validateFields();
					formInstance?.resetFields();
					onCreate(values);
				} catch (error) {
					console.log("Failed:", error);
				}
			}}
		>
			<TodoForm
				onFormInstanceReady={(instance) => {
					setFormInstance(instance);
				}}
				initialValues={initialValues}
			/>
		</Modal>
	);
};

export default TodoModal;
