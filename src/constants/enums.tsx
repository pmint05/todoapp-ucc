export enum Status {
	Todo = "TODO",
	Inprogress = "INPROGRESS",
	Done = "DONE",
}
export enum Priority {
	High = "HIGH",
	Medium = "MEDIUM",
	Low = "LOW",
}
