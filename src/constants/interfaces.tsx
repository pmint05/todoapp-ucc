import { FormInstance } from "antd";
import { Priority, Status } from "./enums";
export interface TodoProps {
	id: string;
	title: string;
	status: Status;
	priority: Priority;
	updateAt: Date;
}
export interface TodoModalProps {
	open: boolean;
	onCreate: (values: TodoProps) => void;
	onCancel: () => void;
	initialValues: TodoProps;
}
export interface TodoFormProps {
	onFormInstanceReady: (instance: FormInstance<TodoProps>) => void;
	initialValues: TodoProps;
}
export interface TaskProps {
	key: number;
	todo: TodoProps;
	onEdit: (todo: TodoProps) => void;
	onDelete: (todo: TodoProps) => void;
	markDone: (id: string) => void;
	markInprogress: (id: string) => void;
}
export interface Filter {
	priority: Priority[];
	status: Status[];
}
