import { useEffect, useState } from "react";

export const TestComponent = ({ setter: setA, value: a }: any) => {
	const [extra, setExtra] = useState(10);
	const add = () => {
		setA(a + 1);
	};
	const minus = () => {
		setA(a - 1);
	};
	useEffect(() => {
		return () => {
			console.log("distroyed");
		};
	}, []);
	useEffect(() => {
		console.log("a changed");
	}, [a]);
	useEffect(() => {
		setExtra(extra + 1);
		console.log("change");
	}, [a]);

	return (
		<div>
			<center>
				<div className="grid grid-cols-2 gap-10">
					<h1 className="font-bold text-7xl p-10 my-8 text-blue-500">
						{a}
					</h1>
					<h1 className="font-bold text-7xl p-10 my-8 text-blue-500">
						{extra}
					</h1>
				</div>
				<div className="grid grid-cols-2 gap-4 text-5xl font-back">
					<button
						className="text-red-500 bg-gray-200 p-2 hover:bg-red-300 active:scale-95 transition-all"
						onClick={minus}
					>
						-
					</button>
					<button
						className="text-green-500 bg-gray-200 p-2 hover:bg-green-300 active:scale-95 transition-all"
						onClick={add}
					>
						+
					</button>
				</div>
			</center>
		</div>
	);
};
