import { useState, useEffect } from "react";
import Task from "./components/Task";
import {
	Button,
	Collapse,
	CollapseProps,
	ConfigProvider,
	Empty,
	Select,
	SelectProps,
} from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { TodoProps, Filter } from "./constants/interfaces";
import { Priority, Status } from "./constants/enums";
import useLocalStorage from "./hooks/useLocalStorage";
import { TodoModal } from "./components";

const priorityOptions: SelectProps["options"] = [
	{
		label: "High",
		value: Priority.High,
	},
	{
		label: "Medium",
		value: Priority.Medium,
	},
	{
		label: "Low",
		value: Priority.Low,
	},
];
const statusOptions: SelectProps["options"] = [
	{
		label: "Todo",
		value: Status.Todo,
	},
	{
		label: "Inprogress",
		value: Status.Inprogress,
	},
];
function Todo() {
	const [todos, setTodos] = useLocalStorage<TodoProps[]>("todos", []);
	const [filterdTodos, setFilteredTodos] = useLocalStorage<TodoProps[]>(
		"todos",
		[]
	);
	const [doneTodos, setDoneTodos] = useLocalStorage<TodoProps[]>("done", []);
	const [open, setOpen] = useState(false);
	const [editingId, setEditingId] = useState<string>("");
	const [filter, setFilter] = useState<Filter>({
		priority: [],
		status: [],
	});
	useEffect(() => {
		const { priority, status } = filter;
		if (!priority.length && !status.length) return setFilteredTodos(todos);
		const tmp = todos.filter((item, index) => {
			const priorityMatch = priority.length
				? priority.includes(item.priority)
				: true;
			const statusMatch = status.length
				? status.includes(item.status)
				: true;
			return priorityMatch && statusMatch;
		});
		setFilteredTodos(tmp);
	}, [filter]);
	const handlePriorityChange = (value: Priority[]) => {
		setFilter({
			...filter,
			priority: value,
		});
	};
	const handleStatusChange = (value: Status[]) => {
		setFilter({
			...filter,
			status: value,
		});
	};
	const [initialValues, setInitialValues] = useState<TodoProps>({
		id: "",
		title: "",
		priority: Priority.Medium,
		status: Status.Todo,
		updateAt: new Date(),
	});
	const onCreate = (values: TodoProps) => {
		console.log("Received values of form: ", values);
		if (editingId) {
			const newTodo = [...todos];
			const editingTodo = newTodo.find((todo) => todo.id === editingId);
			if (editingTodo) {
				editingTodo.title = values.title;
				editingTodo.priority = values.priority;
				editingTodo.status = values.status;
				editingTodo.updateAt = new Date();
			}
			setTodos(newTodo);
			setEditingId("");
			setInitialValues({
				id: "",
				title: "",
				priority: Priority.Medium,
				status: Status.Todo,
				updateAt: new Date(),
			});
			return setOpen(false);
		}
		values.id = Math.random().toString(36).substr(2, 9);
		setTodos((prev) => [...prev, values]);
		setFilteredTodos((prev) => [...prev, values]);
		setOpen(false);
	};
	const onCancel = () => {
		setOpen(false);
		setInitialValues({
			id: "",
			title: "",
			priority: Priority.Medium,
			status: Status.Todo,
			updateAt: new Date(),
		});
	};
	const onEdit = (todo: TodoProps) => {
		setOpen(true);
		setEditingId(todo.id);
		setInitialValues(todo);
	};
	const onDelete = (todo: TodoProps) => {
		const newTodo = todos.filter((item) => item.id !== todo.id);
		setTodos(newTodo);
		setFilteredTodos(newTodo);
	};
	const markDone = (id: string) => {
		const newTodo = [...todos];
		const todo = newTodo.find((todo) => todo.id === id);
		if (todo) {
			todo.status == Status.Done
				? (todo.status = Status.Todo)
				: (todo.status = Status.Done);
		}
		setTodos(newTodo);
	};
	const markInprogress = (id: string) => {
		const newTodo = [...todos];
		const todo = newTodo.find((todo) => todo.id === id);
		if (todo) {
			todo.status == Status.Inprogress
				? (todo.status = Status.Todo)
				: (todo.status = Status.Inprogress);
		}
		setTodos(newTodo);
	};
	const collapseItem: CollapseProps["items"] = [
		{
			key: "1",
			label: "Done todos",
			style: {
				fontWeight: "600",
				backgroundColor: "#ffffffac",
				borderRadius: "10px",
				border: "1px solid #0000000f",
			},
			children: (
				<ul className="flex flex-col justify-center items-center gap-3 mb-8">
					{todos.filter((todo) => todo.status === Status.Done)
						.length > 0 ? (
						todos
							.filter((todo) => todo.status === Status.Done)
							.map((todo, index) => {
								return (
									<Task
										key={index}
										todo={todo}
										onEdit={onEdit}
										onDelete={onDelete}
										markDone={markDone}
										markInprogress={markInprogress}
									/>
								);
							})
					) : (
						<Empty
							className="mt-20"
							image={Empty.PRESENTED_IMAGE_SIMPLE}
							description={<span>No todo found.</span>}
						/>
					)}
				</ul>
			),
		},
	];
	return (
		<main className="w-full min-h-screen">
			<header className="p-4">
				<center>
					<h1 className="text-2xl font-bold">Todo App</h1>
				</center>
			</header>
			<section className="flex flex-col items-center justify-center mx-auto">
				{todos.length > 0 && (
					<div className="z-10 flex gap-2 items-center md:min-w-[410px] xs:min-w-[320px] min-w-full mb-4 sticky top-8 bg-white bg-opacity-50 p-2 xs:rounded-lg border backdrop-blur-sm">
						<Select
							mode="multiple"
							allowClear
							style={{ width: "100%" }}
							placeholder="Select Priority"
							onChange={handlePriorityChange}
							options={priorityOptions}
						/>
						<Select
							mode="multiple"
							allowClear
							style={{ width: "100%" }}
							placeholder="Select Status"
							onChange={handleStatusChange}
							options={statusOptions}
						/>
					</div>
				)}
				<ul className="flex flex-col justify-center items-center gap-3 mb-24">
					<ConfigProvider
						theme={{
							components: {
								Collapse: {
									contentBg: "#transparent",
									contentPadding: "10px 10px !important",
									// headerBg: "transparent",
									headerPadding: "5px 10px !important",
								},
							},
						}}
					>
						<Collapse
							className="md:w-[420px] xs:w-[330px] w-[310px]"
							size="large"
							items={collapseItem}
							bordered={false}
						></Collapse>
					</ConfigProvider>
					{filterdTodos.length > 0 ? (
						filterdTodos
							.filter((todo) => todo.status !== Status.Done)
							.map((todo, index) => {
								return (
									<Task
										key={index}
										todo={todo}
										onEdit={onEdit}
										onDelete={onDelete}
										markDone={markDone}
										markInprogress={markInprogress}
									/>
								);
							})
					) : (
						<Empty
							className="mt-20"
							image={Empty.PRESENTED_IMAGE_SIMPLE}
							description={<span>No todo found.</span>}
						/>
					)}
				</ul>
				<div className="w-full fixed bottom-0 p-2 bg-white drop-shadow-lg flex items-center justify-center md:w-fit md:p-0 md:bg-transparent md:bottom-12 left-1/2 -translate-x-1/2">
					<Button
						type="primary"
						onClick={() => setOpen(true)}
						className="flex items-center justify-center p-4 rounded-full md:h-16 md:w-16 h-14 w-14 text-3xl hover:bg-blue-300 active:scale-95 focus:ring-4 focus:ring-blue-300 transition-all opacity-50 hover:opacity-100"
					>
						<PlusOutlined />
					</Button>
				</div>
				<TodoModal
					open={open}
					onCreate={onCreate}
					onCancel={onCancel}
					initialValues={initialValues}
				/>
			</section>
			<div className="bg fixed top-0 left-0 right-0 bottom-0"></div>
		</main>
	);
}

export default Todo;
