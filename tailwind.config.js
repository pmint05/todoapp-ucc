/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ["./src/**/*.{html,js,tsx,ts,jsx}"],
	theme: {
		extend: {
			screens: {
				xs: "375px",
			},
		},
	},
	plugins: [],
};
